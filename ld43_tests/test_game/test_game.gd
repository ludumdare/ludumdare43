extends Node2D

export (int) var gifts = 10
export (int) var milk = 1
export (int) var cookies = 1
export (int) var coal = 0
export (int) var lives = 3
export (int) var elves = 3
export (int) var spirit = 1000


func on_level_failed():
	print("level failed")
	
	
func _process(delta):
	
	var le = Resources.level
	var li = Resources.lives
	var sc = Resources.score
	var bo = Resources.bonus
	var gi = Resources.gifts
	var mi = Resources.milk
	var co = Resources.cookies
	var cl = Resources.coal
	var sp = Resources.spirit
	var ba = Resources.bag
	var el = Resources.elves
	var se = $Game.start_seed
	
	$Debug/Label.text = "level:%s \nlives: %s \nscore: %s \nbonus: %s \ngifts: %s \nmilk: %s \ncookies: %s \ncoal: %s \nspirit: %s \nbag: %s \nelves: %s \nseed: %s" \
		% [le, li, sc, bo, gi, mi, co, cl, sp, ba, el, se]
	

func init():
	Logger.trace("test_game.init")
		
	Resources.level = 0
	Resources.level_gifts = gifts
	Resources.gifts = 0
	Resources.level_milk = milk
	Resources.milk = 0
	Resources.level_cookies = cookies
	Resources.cookies = 0
	Resources.level_coal = coal
	Resources.coal = 0
	Resources.spirit = spirit
	Resources.lives = lives
	Resources.elves = elves
	Resources.spirit_max = 100
	Resources.bonus_max = 10
	
	
func _ready():
	
	init()
	$Game.init()
	$Game.start()
	
	Resources.connect("level_failed", self, "on_level_failed")


func _on_Reset_pressed():
	
	$Game.stop()
	$Game.reset()
	init()
	$Game.init()
	$Game.start()
	
