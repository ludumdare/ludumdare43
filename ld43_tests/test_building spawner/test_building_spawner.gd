extends Node2D

onready var spawner = $BuildingSpawner

func _process(delta):
	var vel = Vector2(1, 0) * 50 * delta
	$Target.position += vel
		

func _ready():
	spawner.init($Target)
	spawner.start()
