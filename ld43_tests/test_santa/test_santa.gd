extends Node2D

export (int) var gifts = 20
export (int) var milk = 5
export (int) var cookies = 5
export (int) var coal = 1
export (int) var elves = 3
export (int) var SPEED = 5

func _ready():
	
	Resources.elves = 3
		
	$LevelBuilder.build(0, gifts, milk, cookies, coal)
	
	
	
	$Santa.init($Sleigh)
	$Sleigh.init()
	
	$Santa.start()
	$Sleigh.start()
	
	
	Resources.emit_signal("game_start", $Sleigh)
