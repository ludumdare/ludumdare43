extends Node2D

export (int) var gifts = 20
export (int) var milk = 5
export (int) var cookies = 5
export (int) var coal = 1
export (int) var SPEED = 5
export (int) var level = 1

const SPEED_FACTOR = 100

func _process(delta):
	
	if Input.is_mouse_button_pressed(BUTTON_RIGHT):
		if get_viewport().get_mouse_position().x < 240:
			$Camera2D.position += Vector2(-1, 0) * SPEED * SPEED_FACTOR * delta
		else:
			$Camera2D.position += Vector2(1, 0) * SPEED * SPEED_FACTOR * delta
			
func _ready():
	$LevelBuilder.build(level, gifts, milk, cookies, coal)


func _on_Reset_pressed():
	$LevelBuilder.reset()


func _on_Build_pressed():
	$LevelBuilder.reset()
	$LevelBuilder.build(0, gifts, milk, cookies, coal)
