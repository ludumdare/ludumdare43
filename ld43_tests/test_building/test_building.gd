extends Node2D

onready var tilemap = $TileMap

var building

func _generate_building():
	Logger.trace("test_building._generate_building")
	
	tilemap.clear()
	
	if building:
		building.queue_free()
		
	building = Resources.Building.instance()
	add_child(building)
	building.generate(tilemap, position)
	
	
func _process(delta):
	
	if Input.is_action_just_pressed("game_fire"):
		_generate_building()


func _ready():
	Logger.trace("test_building._ready")
	_generate_building()	
