extends Node2D

func _ready():
	
	var tween1 = Tween.new()
	
	add_child(tween1)
	
	tween1.interpolate_property($Character, "position:x", $Character.position.x, $Character.position.x + 100, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween1.interpolate_property($Character, "position:y", $Character.position.y, $Character.position.y + 50, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween1.start()
	yield(tween1, "tween_completed")
	

