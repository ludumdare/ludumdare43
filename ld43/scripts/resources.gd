extends Node

onready var BuildingTileResource = preload("res://ld43/scenes/buildings/building_tile_resource.tscn")
onready var Building = preload("res://ld43/scenes/buildings/building.tscn")
onready var Window = preload("res://ld43/scenes/buildings/window.tscn")
onready var Pickup = preload("res://ld43/scenes/pickups/pickup.tscn")
onready var StreetSign = preload("res://ld43/scenes/game/street_sign.tscn")
onready var Gift = preload("res://ld43/scenes/pickups/gift.tscn")
onready var Milk = preload("res://ld43/scenes/pickups/milk.tscn")
onready var Cookie = preload("res://ld43/scenes/pickups/cookie.tscn")
onready var Coal = preload("res://ld43/scenes/pickups/coal.tscn")

onready var SoundHey = preload("res://ld43/audio/hey.tscn")

signal window_click(window)
signal window_click_elf(window)
signal pickup_item(item)
signal change_score()
signal change_bonus()
signal change_cookies()
signal change_coal()
signal change_gifts()
signal change_milk()
signal change_spirit()
signal change_lives()
signal empty_bag()
signal level_complete()
signal level_failed()
signal game_over()
signal you_win()
signal get_ready()
signal game_start()
signal recall_start()
signal recall_end()
signal hide_messages()
signal replay_level()

# global vars
var score = 0
var hiscore = 0
var lives = 0
var milk = 0
var cookies = 0
var coal = 0
var elves = 0
var gifts = 0
var bag = 0
var bonus = 0
var spirit = 0
var spirit_max = 0
var bonus_max = 10
var speed = 0
var street = ""

var level = 0
var level_gifts = 0
var level_milk = 0
var level_cookies = 0
var level_coal = 0


var btr

func get_building_tile_resource():
	return btr
	
func _ready():
	btr = BuildingTileResource.instance()
	add_child(btr)
	
	
func _init():
	Logger.set_logger_level(Logger.LEVEL_ALL)
	Logger.trace("game._init")
	randomize()