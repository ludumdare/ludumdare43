extends Node2D

func _ready():
	pass


func _on_Play_pressed():
	get_tree().change_scene("res://ld43/scenes/play/play.tscn")


func _on_Quit_pressed():
	get_tree().quit()


func _on_About_pressed():
	get_tree().change_scene("res://ld43/menus/about/about.tscn")
