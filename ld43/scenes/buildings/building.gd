"""
	Class: Building
	
	Remarks:
		
		Create a Building in the Game on the 
		provided Tilemap.
		
		Spawn Points will be added for each
		Window in the Building.
		
"""

extends Node2D

export (Vector2) var point_offset


var tilemap
var spawn_location
var spawn_points = [] setget , _get_spawn_points
var windows = []
var testing = false
var tile_resource
var width
var height

const CELL_SIZE = 40

func add_items(items):
	Logger.trace("building.add_items")
	print(items)
	
	
func generate(tilemap, location):
	Logger.trace("building.generate")
	
	self.tilemap = tilemap
	
	var r = int(floor(rand_range(0, 4)))

	match r:

		0:	generate_building(location, "b0")
		1:	generate_building(location, "b1")
		2:	generate_house(location, "h0")
		3:	generate_house(location, "h1")
			
		
func generate_building(location, style):
	Logger.trace("building.generate_building")
	
	# get width and height
	var w = floor(rand_range(2, 5))
	var h = floor(rand_range(1, 3))
	
	width = w
	height = h
	
	location += Vector2(0, 6 - (h * 2))
	
	# roof
	_add_building_roof(w, location, style)
	
	# walls
	for y in range(h * 2):
		_add_building_wall(w, location + Vector2(0, y + 1), style)
		pass
		
	# windows and spawn points
	_add_building_windows(w - 1, h, location)
	
	
func generate_house(location, style):
	Logger.trace("building.generate_house")
	
	# get width and height
	var w = 4
	var h = floor(rand_range(1, 3))
	width = w
	height = h
	
	location += Vector2(0, 6 - (h * 2))
	
	# roof
	_add_house_roof(w, location, style)
	
	for y in range(h * 2):
		_add_house_wall(w, location + Vector2(0, y + 1), style)	
		
	# add windows
	_add_house_windows(w - 1, h, location)
	
# add roof at location
func _add_house_roof(width, location, style):
	Logger.trace("building._add_house_roof")
	
	for w in range(width):
		var l = Vector2(location.x + w, location.y)
		tilemap.set_cellv(l,  tile_resource.get_tile(style, w))
		
		
func _add_house_wall(width, location, style):
	Logger.trace("building._add_house_wall")
	
	for w in range(width):
		var l = Vector2(location.x + w, location.y)
		tilemap.set_cellv(l,  tile_resource.get_tile(style, w + 4))
	
		
func _add_house_windows(width, height, location):
	Logger.trace("building._add_house_windows")
	
	for h in range(height):
		for w in range(width - 1):
			var point = (Vector2(location.x + w, location.y + h * 2) * CELL_SIZE) + point_offset + (Vector2(CELL_SIZE / 2, 0))
			spawn_points.append(point) 
			var window = Resources.Window.instance()
			add_child(window)
			window.position = point
			windows.append(window)

# add roof at location
func _add_building_roof(width, location, style):
	Logger.trace("building._add_building_roof")
	
	tilemap.set_cellv(location, tile_resource.get_tile(style, 0))
	
	for w in range(width - 1):
		var l = Vector2(location.x + w + 1, location.y)
		tilemap.set_cellv(l,  tile_resource.get_tile(style, 1))
		
	tilemap.set_cellv(Vector2(location.x + width, location.y),  tile_resource.get_tile(style, 3))
	
func _add_building_wall(width, location, style):
	Logger.trace("building._add_building_wall")
	tilemap.set_cellv(location, tile_resource.get_tile(style, 4))
	
	for w in range(width - 1):
		var l = Vector2(location.x + w + 1, location.y)
		tilemap.set_cellv(l,  tile_resource.get_tile(style, 6))
		
	tilemap.set_cellv(Vector2(location.x + width, location.y),  tile_resource.get_tile(style, 5))
	
# add number of windows based on width and height
func _add_building_windows(width, height, location):
	Logger.trace("building._add_building_windows")
	for h in range(height):
		for w in range(width):
			var point = (Vector2(location.x + w, location.y + h * 2) * CELL_SIZE) + point_offset + (Vector2(CELL_SIZE / 2, 0))
			spawn_points.append(point) 
			var window = Resources.Window.instance()
			add_child(window)
			window.position = point
			windows.append(window)
			
		
func _get_spawn_points():
	return spawn_points
	
	
func _ready():
	Logger.trace("building._ready")
	


func _init(tilemap = null, spawn_location = null):
	Logger.trace("building._init")
	tile_resource = Resources.get_building_tile_resource()
