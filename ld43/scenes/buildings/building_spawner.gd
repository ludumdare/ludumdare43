"""
	Class: Building Spawner
	
	Remarks:
		
		Creates Buildings in the Scene based on the
		position in World.
"""

extends Node2D

signal building_added(building)

# building scene
export (PackedScene) var building

# how many buildings to pre generate
export (int) var pre_cache = 5

# how far in front of the current location to build another
export (Vector2) var trigger_offset = Vector2(360, 0)

# next building location 
var next_loc = Vector2(0, 0)
var next_pos = Vector2(0, 0)

# last building location
var last_loc

# a list of the spawned buildings
var buildings = []

# the tilemap
var tilemap

# is the spawner active
var active = false

# target to watch
var target



const CELL_SIZE = 40


func init(target):
	Logger.trace("building_spawner.init")
	
	self.target = target
	
	for i in range(pre_cache):
		_add_building()
	
	
func start():
	Logger.trace("building_spawner.start")
	active= true
	
	
func stop():
	Logger.trace("building_spawner.stop")
	pass
	
	
func pause():
	Logger.trace("building_spawner.pause")
	pass
	
	
func resume():
	Logger.trace("building_spawner.resume")
	pass
	

func _process(delta):
	
	if not active:
		return
		
	if target.position + trigger_offset > next_pos:
		_add_building()
		
			
func _add_building():
	Logger.trace("building_spawner._add_building")
	
	var b = Resources.Building.instance()
	add_child(b)
	b.generate(tilemap, next_loc)
	next_loc += Vector2(b.width + 1, 0)
	next_pos += Vector2(b.width + 1, 0) * CELL_SIZE
	buildings.append(b)
	emit_signal("building_added", b)
	
	
func _ready():
	tilemap = $TileMap
