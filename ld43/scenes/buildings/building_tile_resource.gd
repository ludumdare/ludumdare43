"""
	Class: Building Tile Resource
	
	Remarks:
		
		Will create an object that will have a reference
		to all the tiles that are needed to render the
		tile_resources in the game.
		
"""

extends Node

export (Resource) var tileset

var tile_resources = {}


func get_tileset():
	return tileset
	
	
func get_tile(resource, index):
	var tile = tile_resources[resource][index]
	return tile
	
	
func _ready():
	Logger.trace("building_tile_resource._ready")
	
	for i in tileset.get_tiles_ids():
		
		var tile_name = tileset.tile_get_name(i)
		
		# get the building or house#
		var s0 = tile_name.split("_")
		var key = s0[0]
		
		# add the building
		if not tile_resources.has(key):		
			tile_resources[key] = []
		var res = tile_resources[key]
		
		# add the piece to the resource
		res.append(i)
