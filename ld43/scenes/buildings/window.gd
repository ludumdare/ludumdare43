extends Node2D

onready var Pickup = preload("res://ld43/scenes/pickups/pickup.gd")
var pickup


func add_item(item):
	Logger.trace("window.add_item")
	pickup = item
	add_child(item)
	item.position = Vector2(0, 0)
	
	
func pickup():
	Logger.trace("window.pickup")
	
	if not pickup:
		Resources.bonus = 1
	
	if pickup:
		pickup.collect()
		Resources.emit_signal("pickup_item", pickup)
		pickup.queue_free()
		if pickup.type == 0:
			Resources.gifts += 1
			Resources.bag -= 1
		if pickup.type == 1:
			Resources.milk += 1
		if pickup.type == 2:
			Resources.cookies += 1
		if pickup.type == 3:
			Resources.coal += 1
			Resources.bag -= 1
			
		pickup = null
		return true
		
	Resources.spirit -= 10
	
	return false
	

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		Resources.emit_signal("window_click", self)

	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.is_pressed():
		Resources.emit_signal("window_click_elf", self)
