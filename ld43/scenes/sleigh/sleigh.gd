extends Position2D

export (int) var SPEED = 5

var SPEED_FACTOR = 10

var active = false
var recall = false
var santa 

func init(santa):
	Logger.trace("sleigh.init")
	position = Vector2(-320, 80)
	self.santa = santa
	active = true
	
func start():
	active = true
	show()
	
	
func stop():
	active = false
	hide()

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		Resources.emit_signal("empty_bag")


func _toggle_recall():
	recall = not recall
	if santa.on_sleigh:
		recall = false
	
func _process(delta):
	
	if not active:
		return

	if not recall:		
		position += Vector2(1, 0) * SPEED * SPEED_FACTOR * delta
	else:
		var dir = (santa.position - (position + Vector2(100, 0))).normalized()
		dir = Vector2(dir.x, 0)
		position += dir * SPEED * 5 * SPEED_FACTOR * delta
		Resources.spirit -= 5 * delta
		Resources.emit_signal("change_spirit")
		
	
func _ready():
	Resources.connect("recall_start", self, "_toggle_recall")
	Resources.connect("recall_end", self, "_toggle_recall")

func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		Resources.emit_signal("empty_bag")
