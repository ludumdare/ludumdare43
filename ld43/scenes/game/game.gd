"""
	Class: Game
	
	Remarks:
		
		This controls the Gameplay.
"""

extends Node2D

onready var Pickup = preload("res://ld43/scenes/pickups/pickup.gd")

# other stuff
var last_building
var active 
var start_seed


func init():
	Logger.trace("game.init")
	
	$Santa.init($Sleigh)
	$Sleigh.init($Santa)
	
	# have some kind of table to increase difficulty
	
	
	
func start():
	Logger.trace("game.start")
	_start()
	
func reset():
	Logger.trace("game.reset")
	_reset()

func stop():
	_stop()
	
func _on_pickup_item(item):
	Logger.trace("game._on_pickup_item")
	Resources.bonus += item.bonus
	Resources.score += item.points * Resources.bonus
	Resources.spirit += item.spirit
	
	if Resources.bonus > Resources.bonus_max:
		Resources.bonus = Resources.bonus_max
		
	if Resources.score > Resources.hiscore:
		Resources.hiscore = Resources.score
	

func _reset():
	Logger.trace("game._reset")
	
	$Santa.reset()
	$LevelBuilder.reset()

	
	
func _start():
	Logger.trace("game._start")
	
	$LevelBuilder.build(Resources.level, Resources.level_gifts, Resources.level_milk, Resources.level_cookies, Resources.level_coal)
	
	last_building = $LevelBuilder.next_loc * 40 + Vector2(180, 0)
	print(last_building)
	
	$Santa.start()
	
	$Sleigh.SPEED = Resources.speed
	$Sleigh.start()
	
	$Tween.interpolate_property($Sleigh, "position", $Sleigh.position - Vector2(4096, 0), $Sleigh.position, 3.0,Tween.TRANS_QUAD, Tween.EASE_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")

	active = true	
	
	Resources.emit_signal("get_ready")
	
func _stop():
	Logger.trace("game._stop")
	
	$Santa.stop()
	$Sleigh.stop()
	
	active = false

	
func _process(delta):
	
	if Resources.spirit > Resources.spirit_max:
		Resources.spirit = Resources.spirit_max
		
	if not active:
		return
	
#	print($Sleigh.position, last_building)
	
	if Resources.spirit < 0:
		Resources.emit_signal("level_failed")
		return
	
	if $Sleigh.position > last_building:
		
		if not $Santa.on_sleigh:
			Resources.emit_signal("level_failed")
			return
			
		if Resources.gifts < Resources.level_gifts:
			Resources.emit_signal("level_failed")
			return
		
		if Resources.milk < Resources.level_milk:
			Resources.emit_signal("level_failed")
			return
		
		if Resources.cookies < Resources.level_cookies:
			Resources.emit_signal("level_failed")
			return
		
		if Resources.coal < Resources.level_coal:
			Resources.emit_signal("level_failed")
			return
			
		Resources.emit_signal("level_complete")
		
		
func _ready():
	Logger.trace("game._ready")
	Resources.connect("pickup_item", self, "_on_pickup_item")
	