extends Node2D

export (PackedScene) var Building


onready var Pickup = preload("res://ld43/scenes/pickups/pickup.gd")

onready var tilemap = $TileMap

var level
var gifts
var cookies
var coal
var milk

var pickup_items = []
var buildings = []
var next_loc = Vector2(0, 0)
var building_nodes

func build(level, gifts, milk, cookies, coal):
	Logger.trace("level_builder.build")
	
	var ss = Resources.StreetSign.instance()
	building_nodes.add_child(ss)
	
	self.level = level + 1
	
	self.gifts = gifts
	self.cookies = cookies
	self.coal = coal
	self.milk = milk
	
	gifts += 1 * self.level
	
	if cookies > 0:
		cookies + randi()%self.level + 1
	
	if milk > 0:
		milk + randi()%self.level + 2
		
	if coal > 0:
		coal + floor(rand_range(0, self.level))
		
	_fill_pickup_bag(Pickup.TYPE_GIFT, gifts)
	_fill_pickup_bag(Pickup.TYPE_MILK, milk)
	_fill_pickup_bag(Pickup.TYPE_COOKIE, cookies)
	_fill_pickup_bag(Pickup.TYPE_COAL, coal)
	
	var items_remaining = true
	
	while items_remaining:
		
		var building = _add_building()
		var items = _get_random_items(building.spawn_points.size())
		_add_building_items(building, items)
		items_remaining = pickup_items.size() > 0
		
	
func reset():
	Logger.trace("level_builder.reset")
	
	tilemap.clear()
	
	for building in building_nodes.get_children():
		building.queue_free()
	
	next_loc = Vector2(0, 0)
	buildings = []
	pickup_items = []
	

func _add_building():
	Logger.trace("level_builder._add_building")
	
	var building = Building.instance()
	buildings.append(building)
	building.generate(tilemap, next_loc)
	building_nodes.add_child(building)
	next_loc += Vector2(building.width + 1, 0)
	return building
	
	
func _add_building_items(building, items):
	Logger.trace("level_builder._add_building_items")
	
	var windows = building.windows
	
	for i in range(items.size()):
		var window = _pull_item(windows)
		print(window)
		var item
		
		if items[i] == Pickup.TYPE_GIFT:
			item = Resources.Gift.instance()
			
		if items[i] == Pickup.TYPE_MILK:
			item = Resources.Milk.instance()
			
		if items[i] == Pickup.TYPE_COOKIE:
			item = Resources.Cookie.instance()
			
		if items[i] == Pickup.TYPE_COAL:
			item = Resources.Coal.instance()
			
		item.position += Vector2(0, 125)
		window.add_item(item)
		
	
func _fill_pickup_bag(item, count):
	Logger.trace("level_builder._fill_pickup_bag")
	
	for i in range(count):
		pickup_items.append(item)


func _get_random_items(max_items):
	Logger.trace("level_builder._add_building")

	var items = []
	var r = floor(rand_range(1, max_items))
	
	for i in range(r):
		if pickup_items.size() > 0:
			items.append(_pull_item(pickup_items))
		
	return items


func _pull_item(list):
	var i = randi()%list.size()
	var item = list[i]
	list.remove(i)
	return item
	
func _ready():
	building_nodes = Node.new()
	add_child(building_nodes)
	
