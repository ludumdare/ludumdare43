extends Node2D

export (Vector2) var move_range = Vector2(10, 10)

var parent
var start_pos

func random_move():
	
	var nextpos = start_pos + Vector2(rand_range(move_range.x * -1, move_range.x), rand_range(move_range.y * -1, move_range.y))
	var nextdur = rand_range(0.5, 1.5)
		
	$Tween.interpolate_property(parent, "position", parent.position, nextpos, nextdur, Tween.TRANS_QUAD, Tween.EASE_OUT)
	$Tween.start()
	
func _ready():
	parent = get_parent()
	start_pos = parent.position	
	random_move()

func _init():
	pass
func _on_Tween_tween_completed(object, key):
	random_move()
