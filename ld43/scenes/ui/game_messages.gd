extends Node


func _hide():
	$GameOver.hide()
	$LevelComplete.hide()
	$LevelFailed.hide()
	$YouWin.hide()
	$GetReady.hide()
	
	
func on_level_failed():
	_hide()
	$LevelFailed.show()

func on_level_completed():
	_hide()
	$LevelComplete.show()

func on_game_over():
	_hide()
	$GameOver.show()


func on_you_win():
	_hide()
	$YouWin.show()

func on_get_ready():
	_hide()
	$GetReady.show()
	$Timer.wait_time = 1
	$Timer.start()
	
	
func _ready():
	Resources.connect("level_failed", self, "on_level_failed")
	Resources.connect("level_complete", self, "on_level_completed")
	Resources.connect("get_ready", self, "on_get_ready")
	Resources.connect("hide_messages", self, "_hide")
	Resources.connect("game_over", self, "on_game_over")
	Resources.connect("you_win", self, "on_you_win")
	
func _process(delta):
	$"GetReady/CenterContainer/VBoxContainer/$Level".text = "LEVEL %s" % [Resources.level + 1]


func _on_Timer_timeout():
	_hide()


func _on_Replay_pressed():
	get_tree().change_scene("res://ld43/scenes/play/play.tscn")


func _on_Home_pressed():
	get_tree().change_scene("res://ld43/menus/home/home.tscn")

