extends CanvasLayer

var spirit_val
var anim_bag = false

func on_change_spirit():
	$Spirit.max_value = Resources.spirit_max
	
func _process(delta):
	$Spirit.value = lerp($Spirit.value, Resources.spirit, delta)
	$"Bag/#Bag".text = str(Resources.bag)
	$"Gifts/#Gifts".text = "%s/%s" % [Resources.gifts, Resources.level_gifts]
	$"Milk/#Milk".text = "%s/%s" % [Resources.milk, Resources.level_milk]
	$"Cookies/#Cookies".text = "%s/%s" % [Resources.cookies, Resources.level_cookies]
	$"Coal/#Coal".text = "%s/%s" % [Resources.coal, Resources.level_coal]
	$"#Bonus".text = "x%s" % [Resources.bonus]
	$"Container/$Level".text = "Level %s : %s" % [Resources.level + 1, Resources.street]
	
	$Lives/S1.hide()
	$Lives/S2.hide()
	$Lives/S3.hide()
	
	if Resources.lives > 2:
		$Lives/S3.show()
		
	if Resources.lives > 1:
		$Lives/S2.show()
		
	if Resources.lives > 0:
		$Lives/S1.show()
		
	$"#Score".text = str(Resources.score)
	$"#HiScore".text = str(Resources.hiscore)
	
	if Resources.bag > 0:
		$Bag/AnimationPlayer.play("full")
		anim_bag = false
	else:
		if not anim_bag:
			$Bag/AnimationPlayer.play("empty")
			anim_bag = true

func _ready():
	Resources.connect("change_spirit", self, "on_change_spirit")

func _on_Recall_button_down():
	Resources.emit_signal("recall_start")



func _on_Recall_button_up():
	Resources.emit_signal("recall_end")
