extends Node

var levels = []

export (int) var start_level = 0

func _play(level):
	Logger.trace("play._play")
	
	var l = levels[level]
	
	if not l.has("start_seed"):
		var start_seed = _get_seed(5)
		l.start_seed = start_seed

	Logger.debug("starting seed: %s" % [l.start_seed])
	seed(l.start_seed.hash())
	
	Resources.level_gifts = l.gifts
	Resources.gifts = 0
	Resources.level_milk = l.milk
	Resources.milk = 0
	Resources.level_cookies = l.cookies
	Resources.cookies = 0
	Resources.level_coal = l.coal
	Resources.coal = 0
	Resources.spirit = 50
	Resources.elves = 3
	Resources.spirit_max = 50
	Resources.bonus = 1
	Resources.bonus_max = 10
	Resources.speed = l.speed
	
	$Game.init()
	$Game.start()

	
func _next():
	Logger.trace("play._next")
	Resources.emit_signal("hide_messages")
	$Game.stop()
	$Game.reset()
	$Game.start_seed = null
	Resources.level += 1
	if Resources.level > levels.size() - 1:
		Resources.emit_signal("you_win")
		return
		
	_play(Resources.level)
	
	
func _prev():
	Logger.trace("play._prev")
	Resources.emit_signal("hide_messages")
	$Game.stop()
	$Game.reset()
	$Game.start_seed = null
	Resources.level -= 1
	_play(Resources.level)
	
func _reset():
	Logger.trace("play._reset")
	Resources.emit_signal("hide_messages")
	$Game.stop()
	$Game.reset()
	_play(Resources.level)
	
	
func _on_level_failed():
	Logger.trace("play._on_level_failed")
	$Game.stop()
	Resources.lives -= 1
	
	if Resources.lives > 0:
		$Tween.interpolate_callback(self, 2, "_reset")
		$Tween.start()
	else:
		_on_game_over()
	


func _on_level_complete():
	Logger.trace("play._on_level_completed")
	$Game.stop()
	$Tween.interpolate_callback(self, 2, "_next")
	$Tween.start()
	
	
func _on_game_over():
	print("game over")
	Resources.emit_signal("game_over")
	
func _on_you_win():
	Logger.trace("play._on_you_win")
	Resources.emit_signal("you_win")
	
	
func _get_seed(length):
	Logger.trace("game._get_seed")
	
	var s = "abcdef0123456789"
	var sseed = ""
	for l in range(length):
		sseed += s[rand_range(0, s.length())]
		sseed += s[rand_range(0, s.length())]

	return sseed
	
	
func _on_Reset_pressed():
	Logger.trace("play._on_Reset_pressed")
	_reset()
	

func _on_Next_pressed():
	_next()

func _on_Prev_pressed():
	_prev()
	
	
func _ready():
	Logger.trace("play._ready")
	
	Resources.level = 0
	
	if start_level > 0:
		Resources.level = start_level - 1
		
	Resources.score = 0
	Resources.lives = 3
	
	Resources.connect("level_complete", self, "_on_level_complete")
	Resources.connect("level_failed", self, "_on_level_failed")
	
	_play(Resources.level)

	
func _init():
	Logger.trace("play._init")
	
	
	levels.append( { "level": 1, "gifts": 10, "milk": 0, "cookies": 0, "coal": 1, "speed": 5 })
	levels.append( { "level": 2, "gifts": 15, "milk": 1, "cookies": 0, "coal": 1, "speed": 5 })
	levels.append( { "level": 3, "gifts": 25, "milk": 2, "cookies": 2, "coal": 2, "speed": 6 })
	levels.append( { "level": 4, "gifts": 35, "milk": 2, "cookies": 2, "coal": 2, "speed": 6 })
	levels.append( { "level": 5, "gifts": 40, "milk": 4, "cookies": 4, "coal": 3, "speed": 8 })
	levels.append( { "level": 6, "gifts": 45, "milk": 4, "cookies": 4, "coal": 3, "speed": 8 })
	levels.append( { "level": 7, "gifts": 50, "milk": 6, "cookies": 5, "coal": 4, "speed": 9 })
	levels.append( { "level": 8, "gifts": 55, "milk": 6, "cookies": 5, "coal": 4, "speed": 9 })
	levels.append( { "level": 9, "gifts": 60, "milk": 8, "cookies": 10, "coal": 5, "speed": 10 })
	levels.append( { "level": 10, "gifts": 70, "milk": 8, "cookies": 10, "coal": 5, "speed": 10 })
	
	
