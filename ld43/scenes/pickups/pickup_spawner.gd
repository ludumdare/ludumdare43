extends Node

onready var pickup = preload("res://ld43/scenes/pickups/pickup.tscn")

func spawn(building):
	Logger.trace("pickup_spawner.spawn")
	print(building.spawn_points)
	_add_pickups(building)
	
	
# add pickups on a building
func _add_pickups(building):
	Logger.trace("pickup_spawner._add_pickups")
	
	for s in building.spawn_points:
		var p = pickup.instance()
		add_child(p)
		p.position = s + Vector2(0, 15)
	
	
func _ready():
	Logger.trace("pickup_spawner._ready")
	pass
