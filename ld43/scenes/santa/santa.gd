extends Area2D

enum {
	STATE_NONE,
	STATE_PLAY,
	STATE_MOVE,
	STATE_RIDE
}

var anim = [ "idle1", "idle2" ]

onready var elf = preload("res://ld43/scenes/elf/elf.tscn")

var active = false

var bag_max = 10
var window
var move_queue
var sleigh
var state
var SPEED = 10
var on_sleigh = false
var my_elves_node
var my_elves = []
var world_elves

func fill_bag():
	Resources.bag = bag_max
	

func init(sleigh):
	
	Logger.trace("santa.init")
	
	position = Vector2(0, 0)
	Resources.bag = bag_max
	move_queue = []
	self.sleigh = sleigh
	
	my_elves_node = Node2D.new()
	my_elves_node.name = "my_elves_node"
	sleigh.get_node("Area2D").add_child(my_elves_node)	
	my_elves_node.position += Vector2(-15, 10)
	
	world_elves = Node2D.new()
	world_elves.name = "world_elves"
	get_parent().add_child(world_elves)
	
	# add elves
	for i in range(Resources.elves):
		var e = elf.instance()
#		e.position = Vector2(0,0)
		my_elves_node.add_child(e)
		my_elves.append(e)
		e.position += Vector2(rand_range(-15, 15), 0)
	

func reset():
	
	if my_elves_node:
		my_elves_node.queue_free()
		
	if world_elves:
		world_elves.queue_free()
		
	my_elves = []
	
func start():
	Logger.trace("santa.start")

	on_empty_bag()
	show()	
	active = true
	$IdleTimer.start()
	
	
func stop():
	Logger.trace("santa.stop")
	
	hide()
	active = false
	$IdleTimer.stop()

	
func on_empty_bag():
	Logger.trace("santa.on_empty_bag")
	
	if on_sleigh:
		return
	
#	self.sleigh = sleigh
	
#	var e = elf.instance()
#	e.position = sleigh.position
#	my_elves_node.add_child(e)
#
#	$Tween.interpolate_property(e, "position", e.position, position, 1.0, Tween.TRANS_QUAD, Tween.EASE_OUT)
#	$Tween.start()
#
#	var s = Resources.SoundHey.instance()
#	e.add_child(s)
	
	
	var newpos = sleigh.position
	var tween = Tween.new()
	add_child(tween)
	tween.interpolate_property(self, "position", position, newpos, 0.05, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	remove_child(tween)
	
	fill_bag()
	state = STATE_RIDE
	on_sleigh = true
	
#	sleigh.active = true
	

func on_game_start(sleigh):
	state = STATE_RIDE
	
	
func on_window_click(window):
	
	if not active:
		return 
		
	Logger.trace("santa.on_window_click")
	state = STATE_PLAY
	self.window = window
	on_sleigh = false
	_move_santa(window)


func on_window_click_elf(window):
	
	if not active:
		return 
		
	if not on_sleigh:
		return
		
	if Resources.elves < 1:
		return
		
		
	Logger.trace("santa.on_window_click_elf")
	Resources.elves -= 1
	self.window = window
	_move_elf(window)


func _move_santa(window):
	
	if not active:
		return
		
	Logger.trace("santa._move_santa")
	$AnimationPlayer.play("jump")
	$IdleTimer.stop()
	
	var newpos = window.position + Vector2(0, 22)
	var tween = Tween.new()
	add_child(tween)
	tween.interpolate_property(self, "position", position, newpos, 0.80, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	remove_child(tween)
	
	if Resources.bag > 0:
		if window:
			window.pickup()
#			if window.pickup():
#				Resources.bag -= 1
	
	$AnimationPlayer.play("idle1")
	$IdleTimer.start()


func _move_elf(window):
	
	if not active:
		return
		
	Logger.trace("santa._move_elf")
	
#	var e = elf.instance()
#	e.position = sleigh.position
#	get_parent().add_child(e)

	var q = my_elves[Resources.elves]
	
	var e = elf.instance()
	world_elves.add_child(e)
	
	var s = Resources.SoundHey.instance()
	e.add_child(s)

	e.get_node("AnimationPlayer").play("jump")
	e.get_node("IdleTimer").stop()
	
	var pos = q.global_position
	q.hide()
	
#	q.queue_free()
	
	var newpos = window.position + Vector2(0, 22)
	var tween = Tween.new()
	add_child(tween)
	tween.interpolate_property(e, "position", pos, newpos, 0.80, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	remove_child(tween)
	
	if window:
		window.pickup()
#		if window.pickup():
#			Resources.bag -= 1
	
	e.get_node("AnimationPlayer").play("idle1")
	e.get_node("IdleTimer").stop()
	


func _process(delta):
	
	match state:
		
		STATE_NONE:
			pass
			
		STATE_PLAY:
			pass
			
		STATE_MOVE:
			var target_pos = sleigh.position + Vector2(0, -25)
			var vel = (target_pos - position).normalized() * SPEED * 10 * get_process_delta_time()
			var dis = position.distance_to(target_pos) 
			var arrived = dis < 1
			position += vel
			if arrived:
				state = STATE_RIDE
				
		STATE_RIDE:
			position = sleigh.get_node("Area2D").global_position + Vector2(40, 5)
		

func _ready():
	Resources.connect("window_click", self, "on_window_click")
	Resources.connect("window_click_elf", self, "on_window_click_elf")
	Resources.connect("empty_bag", self, "on_empty_bag")
	Resources.connect("game_start", self, "on_game_start")


func _on_IdleTimer_timeout():
	$AnimationPlayer.play(anim[floor(rand_range(0, anim.size()))])
	$IdleTimer.wait_time = rand_range(0.5, 1.0)
	$IdleTimer.start()


