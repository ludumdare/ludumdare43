extends Position2D

export (NodePath) var Target

var camera_target

func _process(delta):
	position = Vector2(camera_target.position.x, position.y)
	
	
func _ready():
	camera_target = get_node(Target)
