extends Area2D

var anim = [ "idle1"]

func _ready():
	$IdleTimer.wait_time = rand_range(1.5, 2.5)
	$IdleTimer.start()


func _on_IdleTimer_timeout():
	$AnimationPlayer.play(anim[floor(rand_range(0, anim.size()))])
	$IdleTimer.start()
