extends Node2D

export (int) var SPEED
export (NodePath) var target_path


enum {
	STATE_NONE,
	STATE_PLAY,
	STATE_FORWARD,
	STATE_REVERSE,
	STATE_PAUSE
}

var state
var active
var target

func change_state(state):
	self.state = state
	
	
func on_play():
#	Logger.trace("scroller.on_play")
	state = STATE_FORWARD
	

func on_forward():
	var vel = Vector2(1, 0) * SPEED * 10 * get_process_delta_time()
	target.position += vel
	
	
func on_reverse():
#	Logger.trace("scroller.on_reverse")
	var vel = Vector2(-1, 0) * SPEED * 30 * get_process_delta_time()
	target.position += vel
	
	
func on_pause():
#	Logger.trace("scroller.on_pause")
	pass
	
	
func play():
	Logger.trace("scroller.play")
	state = STATE_PLAY		


func reverse():
	Logger.trace("scroller.reverse")
	state = STATE_REVERSE
	
	
func pause():
	Logger.trace("scroller.pause")
	state = STATE_PAUSE	


func _process(delta):
	
	match state:
		
		STATE_PLAY:
			on_play()
			
		STATE_FORWARD:
			on_forward()
			
		STATE_REVERSE:
			on_reverse()
			
		STATE_PAUSE:
			on_pause()
			
			
func _ready():
	Logger.trace("scroller._ready")
	target = get_node(target_path)
	
		
func _init():
	Logger.trace("scroller._init")
	state = STATE_NONE	
