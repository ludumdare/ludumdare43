extends Area2D

var anim = [ "idle1", "idle2", "jump1" ]

func jump_to(pos):
	$Tween.interpolate_property(self, "position", position, pos, 1.0, Tween.TRANS_QUAD, Tween.EASE_OUT)
	$Tween.start()

func _ready():
	$IdleTimer.wait_time = rand_range(0.5, 1.0)
	$IdleTimer.start()


func _on_IdleTimer_timeout():
	$AnimationPlayer.play(anim[floor(rand_range(0, anim.size()))])
	$IdleTimer.wait_time = rand_range(0.5, 1.0)
	$IdleTimer.start()
