extends AudioStreamPlayer2D

func _ready():
	pitch_scale = rand_range(0.9, 1.1)
	play()
